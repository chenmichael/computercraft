local discord = {}

local function createFormBody(data)
	first = true
	body = ""
	for key, value in pairs(data) do
		if first then
			first = false
		else
			body = body.."&"
		end
		body = body..textutils.urlEncode(tostring(key)).."="..textutils.urlEncode(tostring(value))
	end
	return body
end

local function parseResponse(response)
	local content = response.readAll()
	local result, reason = textutils.unserializeJSON(content)
	if not result then print("Deserialization failed:", reason) return nil end
	return result
end

function discord.webhookMessage(webhookId, webhookToken, content, username, avatar_url, tts)
	local url = "https://discord.com/api/webhooks/"..webhookId.."/"..webhookToken.."?wait=true"
	local headers = { ["Content-Type"] = "application/x-www-form-urlencoded" }
	local body = { content = content }
	if username then body.username = username end
	if avatar_url then body.avatar_url = avatar_url end
	if tts then body.tts = tts end
	local response, reason, err = http.post({ url = url, headers = headers, body = createFormBody(body) })
	if not response then print("Request failed:", reason) return nil end
	return parseResponse(response)
end

return discord
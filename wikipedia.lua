local wikipedia = {}

local function request(url)
	local valid, reason = http.checkURL(url)
	if not valid then print("URL is invalid:", reason) return nil end
	local response, reason, err = http.get(url)
	if not response then print("Request failed:", reason) return nil end
	return response.readAll()
end

local function jsonrequest(url)
	local response = request(url)
	if not response then return nil end
	local result, reason = textutils.unserializeJSON(response)
	if not result then print("Deserialization failed:", reason) return nil end
	return result
end

local function first(list) for x,y in pairs(list) do return x,y end return nil, nil end

function wikipedia.search(query)
	local srsearch = textutils.urlEncode(query)
	local url = "http://en.wikipedia.org/w/api.php?action=query&list=search&format=json&srsearch="..srsearch
	local response = jsonrequest(url)
    return response
end

return wikipedia
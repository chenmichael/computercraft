local fuelSlot = 1
local plantdata = { ["ars_nouveau:mana_bloom_crop"] = { name = "Magebloom", maxage = 7, seed = "ars_nouveau:mana_bloom_crop" }
				  , ["minecraft:wheat"] = { name = "Wheat", maxage = 7, seed = "minecraft:wheat_seeds" }
				  }  
local sleepminutes = 30 -- half the average growth time

local function waitForKey(key)
	while true do
		local event, pressed, isHeld = os.pullEvent("key")
		if key == pressed then
			return true
		else
			print("Waiting for", keys.getName(key), "but got", keys.getName(pressed))
		end
	end
end

local function isSeedFor(item, plant)
	if item ~= nil and item.name == plant.seed then
		print("Selected", plant.name, "seeds!")
		return true
	end
	return false
end

local function isSeed(item)
	for id, plant in pairs(plantdata) do
		if isSeedFor(turtle.getItemDetail(), plant) then return true end
	end
	return false
end

local function selectSeedSlot()
	if isSeed(turtle.getItemDetail()) then return true end
	print("No more seeds in slot", turtle.getSelectedSlot())
	for id, plant in pairs(plantdata) do
		for slot=1,16 do
			turtle.select(slot)
			if isSeedFor(turtle.getItemDetail(), plant) then return true end
		end
		print("No more seeds for", plant.name, "available!")
	end
	return false
end

local function requestNewFuel()
	turtle.select(fuelSlot)
	print("Please insert fuel in slot", fuelSlot, "and press ENTER")
	waitForKey(keys.enter)
end

local function emptySlot(slot)
	if turtle.getItemCount(slot) == 0 then return true end

	local success, data = turtle.inspectUp()
	if not success then
		print("Place a chest above the turtle and press ENTER")
		return false
	end
	
	if not data.tags["forge:chests"] then
		print("The", data.name, "above is not a chest, place one there and press ENTER")
		return false
	end
	
	turtle.select(slot)
	if not turtle.dropUp() then
		print("Inventory above me is full! Make space and press ENTER")
		return false
	end
	return true
end

local function emptyInventory()	
	for i=2,16 do
		while not emptySlot(i) do waitForKey(keys.enter) end
	end
end

local function hasFuel(minFuel)
	return turtle.getFuelLevel() >= minFuel
end

local function ensureFueled(minFuel)
	if not minFuel then minFuel = 1 end
	if hasFuel(minFuel) then return true end

	local previousSlot = turtle.getSelectedSlot()
	turtle.select(fuelSlot)
	turtle.refuel(1)
	while turtle.getFuelLevel() < minFuel do
		turtle.select(fuelSlot)
		if not turtle.refuel(1) then
			requestNewFuel()
		end
	end
	turtle.select(previousSlot)
	return true
end

local function moveForward(amount)
	if not amount then amount = 1 end
	for i=1,amount do
		ensureFueled()
		while not turtle.forward() do
			print("Movement is blocked, please make way!")
			sleep(5)
		end
	end
end

local function replant()
	if not selectSeedSlot() then
		print("No more seeds in inventory!")
		return false
	end

	if not turtle.placeDown() then
		print("Could not replant the seed!")
		return false
	end
	
	print("Seed was replanted!")
	return true
end

local function harvest()
	local success, data = turtle.inspectDown()
	if not success then
		print("No plant below!")
		return replant()
	end	
	local plant = plantdata[data.name]
	
	if not plant then
		print(data.name, "is not a whitelisted plant!")
		return false
	end
	local age = data.state.age
	print(plant.name, "below has age", age)
	
	if age < plant.maxage then
		print(plant.name, "is not ready yet!")
		return false
	end
	print(plant.name, "is fully grown!")
	
	if not turtle.digDown() then
		print("Harvesting", plant.name, "failed!")
		return false
	end
	print(plant.name, "was harvested!")
	
	return replant()
end

local function sweepRow()
	local harvested = 0
	for pos=1,16 do
		if harvest() then
			harvested = harvested + 1
		end
		if pos == 16 then break end
		moveForward()
	end
	return harvested
end

local function sweepRows()
	local harvested = 0
	for pos=1,8 do
		harvested = harvested + sweepRow()
		turtle.turnRight()
		moveForward()
		turtle.turnRight()
		harvested = harvested + sweepRow()
		if pos == 8 then break end
		turtle.turnLeft()
		moveForward()
		turtle.turnLeft()
	end
	turtle.turnRight()
	moveForward(15)
	turtle.turnRight()
end

print("Mining Turtle Farm^")
while true do
	ensureFueled(256)
	sweepRows()
	emptyInventory()
	print("Sleeping for", sleepminutes, "minutes!")
	sleep(sleepminutes * 60)
end
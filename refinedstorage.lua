local function chunkString(value, chunkSize)
	if not chunkSize then chunkSize = 10000 end
	local length = value:len()
	local total = math.ceil(length / chunkSize)
	local chunks = {}
	local i = 1
	for i=1,total do
		local pos = 1 + ((i - 1) * chunkSize)
		chunks[i] = value:sub(pos, pos + chunkSize - 1)
	end
	return total, chunks
end

local function main()
	print("Connecting to the socket server...")
	local socket, reason = http.websocket("ws://ws.cnml.de:8080")
	if not socket then error("Socket server could not be reached: "..reason) end
	print("Connection successful!")
	
	local function sendJson(message)
		return socket.send(textutils.serializeJSON(message))
	end
	
	sendJson({ role = "rs" })

	local function handleRequest(parsed)
		local rs = peripheral.find("rsBridge")
		if not rs then return false, "No RS Bridge attached to the computer!" end

		if parsed.method == "energyusage" then
			return true, tostring(rs.getEnergyUsage())
		elseif parsed.method == "energystorage" then
			return true, tostring(rs.getEnergyStorage())
		elseif parsed.method == "listitems" then
			return true, textutils.serializeJSON(rs.listItems())
		elseif parsed.method == "listfluids" then
			return true, textutils.serializeJSON(rs.listFluids())
		end
		
		return false, ("Invalid method invocation: "..parsed.method.."!")
	end

	local function sendResponse(id, result, success)
		if not success then
			sendJson({ id = id, result = result, success = success })
			return
		end
		
		local total, chunks = chunkString(result)
		for i, chunk in pairs(chunks) do
			socket.send(textutils.serializeJSON({ id = id, result = chunk, chunk = i, total = total, success = success }))
		end
	end

	local function handleMessage(message)
		local parsed, reason = textutils.unserializeJSON(message)
		if not parsed then printError("Deserialization failed: "..reason..", in messsage "..message) return end
		
		if parsed.type == "request" then
			local success, result = handleRequest(parsed)
			sendResponse(parsed.id, result, success)
			if not success then
				printError("Error when answering", parsed.method, "method:", result)
			end
			return
		end
		
		sendResponse(parsed.id, "No handler for messages of type "..parsed.type.."!", false)
		printError("No handler for messages of type", parsed.type)
	end

	while true do
		local message, binary = socket.receive(0.5)
		if not not message and not binary then
			handleMessage(message)
		end
	end
end

while true do
	local status, err = pcall(main)
	if not status then
		printError("Uncaught error:", err)
		print("Sleeping for 5 seconds and retrying!")
		sleep(5)
	end
end
local sleepminutes = 2
local rituals = { sunrise = { tablet = "ars_nouveau:ritual_sunrise", name = "Ritual of Sunrise", augments = {} } -- augments not implemented
                , moonfall = { tablet = "ars_nouveau:ritual_moonfall", name = "Ritual of Moonfall", augments = {} }
                }
local automata = peripheral.find("weakAutomata")

local function isItemSelected(item)
	if not item then
		return name == nil
	else
		return item.name == name
	end
end

local function selectItem(name)
	if isItemSelected(turtle.getItemDetail()) then return true end
	for slot=1,16 do
		turtle.select(slot)
		if isItemSelected(turtle.getItemDetail()) then return true end
	end
		
	-- Additionally check external inventories
	print("Not found internally! Looking in external inventories.")
	for _, inv in pairs({ peripheral.find("inventory") }) do
		for slot, item in pairs(inv.list()) do
			if isItemSelected(item)) then
				-- select free slot
				requestItem(nil)
				local transferred = inv.pullItems(peripheral.getName(inv), slot, nil, turtle.getSelectedSlot())
				if transferred > 0 then return true end
			end
		end
	end
	return false
end

local function requestItem(name)
	print("Item", name, "was requested!")
	while true do
		if selectItem(name) then return true end
		if not name then
			printError("Please make one slot empty!")
		else
			printError(name, " was not found. Give me moar!")
		end
		os.pullEvent("turtle_inventory")
	end
end

local function ensureOff(side)
	if rs.getOutput(side) then
		print("Oops, the output was still one before pulse!")
		rs.setOutput(side, false)
		sleep(0.5)
	end
end

local function redstonePulse(side, length)
	if not length then length = 0.5 end
	ensureOff(side)
	rs.setOutput(side, true)
	sleep(length)
	rs.setOutput(side, false)
end

local function ensureFuel(minAmount)
	if not minAmount then minAmount = 1 end
	while turtle.getFuelLevel() < minAmount do
		requestItem("minecraft:coal")
		local success, reason = turtle.refuel()
		if not success then printError("Refueling failed:", reason) end
	end
end

local function performRitual(ritual)
	print("Performing", ritual.name)
	requestItem(ritual.tablet)
	turtle.place()
	if not automata then
		redstonePulse("bottom")
	else
		requestItem(nil) -- request empty hand
		ensureFuel()
		automata.useOnBlock()
	end
end

local function makeDay()
	performRitual(rituals.sunrise)
end

local function isNight()
	darkness = rs.getAnalogInput("top")
	return darkness > 3
end

local function waitForNight()
	while true do
		os.pullEvent("redstone")
		if isNight() then break end
		print("Still not night...")
	end
end

local function main()
	while true do
		print("Waiting for nighttime!")
		waitForNight()
		makeDay()
		print("Sleeping for", sleepminutes, "minutes!")
		sleep(sleepminutes * 60)
	end
end

print("Ars Nouveau Daylight Keeper")
print("Place computer in front of a Ritual Brazier.")
print("Place an inverted daylight sensor on top of the computer.")
if not automata then
	print("Place an interaction spell turret facing the brazier with redstone signal on the bottom side!")
end
main()
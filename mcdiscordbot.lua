local connectionUri = "ws://ws.cnml.de:8081"
local waitSeconds = 5
local scriptFile = "remotecode"

local function runScript(socket, code)
	local script, reason = loadstring(code)
	if not script then error("Script was not interpreted: "..reason) end
	socket.close()
	local success, reason = pcall(script)
	if not success then error("Script execution failed: "..(reason.message or reason)) end
end

local function socketClient()
	print("Connecting to code host "..connectionUri.."...")
	local socket, reason = http.websocket(connectionUri)
	if not socket then error("Host could not be reached: "..reason) end
	print("Connection successful!")

	while true do
		socket.send("getcode")
		local code, binary = socket.receive()
		if not not code and not binary then
			print("Received script from server!")
			local success, reason = pcall(function() runScript(socket, code) end)
			if not success then
				printError("Running the script failed: "..reason)
				socket.send("error="..reason)
				socket.close()
			end
		end
	end
end

local function main()
	while true do
		local status, error = pcall(socketClient)
		if status then break end
		printError("An uncaught exception was raised:", error)
		printError("Restarting in", waitSeconds, "seconds...")
		sleep(waitSeconds)
	end
end

main()

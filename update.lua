args = {...}
local program = args[1]
local programs = { farm = { name = "Wheat Farm", key = "LReZB4Dv" }
				 , update = { name = "Pastebin Program Updater", key = "H75Wz4BX" }
				 }

local function updateProgram(program)
	if not program then
		print("No program was given in CLI!")
		return false
	end
	
	local progData = programs[program]
	if not progData then
		print("Program", program, "is not registered with a key!")
		return false
	end
	local progName = progData.name
	local key = progData.key
	
	print("Updating", progName, "with key", key)
	shell.run("delete", program)
	if not shell.run("pastebin", "get", key, program) then
		print("Something went wrong!")
	end
	print("Update successful!")
end

updateProgram(program)
local chat = peripheral.find("chatBox")
if not chat then error("No chatbox attached!") end

print("Connecting to the socket server...")
local socket, reason = http.websocket("ws://ws.cnml.de:8080")
if not socket then error("Socket server could not be reached: "..reason) end
print("Connection successful!")
socket.send("{\"roles\":[\"chat\"]}")

local function handleMessage(message)
	local parsed, reason = textutils.unserializeJSON(message)
	if not parsed then print("Deserialization failed: "..reason..", in messsage "..message) return end
	
	if parsed.type == "text" then
		chat.sendMessage(parsed.message, parsed.author)
		return
	end
	
	print("No handler for messages of type", parsed.type)
end

while true do
	message, binary = socket.receive(1)
	if not not message and not binary then
		handleMessage(message)
	end
end